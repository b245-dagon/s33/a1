// console.log("REST API")









fetch("https://jsonplaceholder.typicode.com/todos",{

		method: "GET"

})
.then(response => response.json())
.then(data => {
	let title = data.map (element => element.title)
	console.log(title)
})

// GET SINGLE TO DO LIST
fetch("https://jsonplaceholder.typicode.com/todos/1" ,{

	method: "GET",
	 
})
.then(response => response.json())
.then(result => console.log(result));

// GET TITLE AND STATUS
fetch("https://jsonplaceholder.typicode.com/todos/1" ,{

	method: "GET",
	 
})
.then(response => response.json())
.then(result => console.log(`The item "${result.title}"" on the list has a status of ${result.completed}`));




fetch("https://jsonplaceholder.typicode.com/todos/1" ,{

	method: "GET",
	 
})
.then(response => response.json())
.then(result => console.log(result));


// POSTING
fetch("https://jsonplaceholder.typicode.com/todos" 
	,{
		method: "POST",
		headers:{
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			title: "Create to do list item",
			userId:1
		})
})
.then(response => response.json())
.then(result => console.log(result));


// PUT METHOD
fetch("https://jsonplaceholder.typicode.com/todos/1",
	{
		method : "PUT",
		headers : {"Content-Type": "application/json"},
		body: JSON.stringify({
			title: "Update to do list item",
			description: "To update the my to do list with a different data structure",
			status: "Pending",
			datecompleted: "Pending",
			userId: 1
		})


})
.then(response => response.json())
.then(result => console.log(result));


// PATCH METHOD

fetch("https://jsonplaceholder.typicode.com/todos/1",
		{
			method : "PATCH",
			headers : {"Content-Type": "application/json"},
			body: JSON.stringify({
			
			title: "Update to do list item",
			description: "To update the my to do list with a different data structure",
			status: "Complete",
			datecompleted: "01/31/23",
			userId: 1
			
		})

})
.then(response => response.json())
.then(result => console.log(result));
 
//  DELETE METHOD
fetch("https://jsonplaceholder.typicode.com/todos/1",

		{
	method: "DELETE",

}).then(response => response.json())
.then(result => console.log(result));